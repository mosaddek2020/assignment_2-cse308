public class FactoryProducer {

    public static AbstructFactory getFactory(String choice){
        if(choice.equalsIgnoreCase("Printer")){
            return new PrintFactory();
        }else if(choice.equalsIgnoreCase("display")){
            return  new DisplayFactory();
        }
        else return  null;
    }
}
