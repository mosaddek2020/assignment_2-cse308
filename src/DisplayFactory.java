public class DisplayFactory extends AbstructFactory{

    @Override
    IPrinter getPrinter(String  PrinterType) {
        return null;
    }

    @Override
    IDisplay getDisplay(String DisplayType) {

        if (DisplayType == null) {

            return null;

        }
        if (DisplayType.equalsIgnoreCase("LRD")) {
            return new LowResolutionDisplay();
        } else if (DisplayType.equalsIgnoreCase("HRD")) {
            return new HighResolutionDisplay();

        }
        return null;
    }

}
