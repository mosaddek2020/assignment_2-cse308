public class PrintFactory extends AbstructFactory {


    @Override
    IDisplay getDisplay(String Display) {
        return null;
    }

    @Override
    IPrinter getPrinter(String PrinterType) {

        if (PrinterType == null) {

            return null;

        }
        if (PrinterType.equalsIgnoreCase("LRP")) {
            return new LowResolutionPrinter();
        } else if (PrinterType.equalsIgnoreCase("HRP")) {
            return new HighResolutionPrinter();

        }
        return null;
    }
}