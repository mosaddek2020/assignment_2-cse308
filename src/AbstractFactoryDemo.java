public class AbstractFactoryDemo {
    public static void main(String []args){

        //get Printer drive factory
        AbstructFactory printFactory = FactoryProducer.getFactory("printer");

      //getting an object of LowResolutionPrintDriver
        IPrinter printerDriver1 = printFactory.getPrinter("LRP");
        printerDriver1.ShowPrinter();

        //getting an object of High Resolution PrintDriver
        IPrinter printerDriver2 = printFactory.getPrinter("HRP");
        printerDriver2.ShowPrinter();



        //get Display drive factory
        AbstructFactory displayFactory = FactoryProducer.getFactory("display");

        //getting an object of LowResolutionDisplayDriver
        IDisplay displaydriver1 = displayFactory.getDisplay("LRD");
        displaydriver1.ShowDisplay();


        //getting an object of HighResolutionDisplayDriver
        IDisplay displaydriver2 = displayFactory.getDisplay("HRD");
        displaydriver2.ShowDisplay();


    }
}
